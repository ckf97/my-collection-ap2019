# Mini Exercise 10 by Cecilie Frandsen

**Individual:**

[RunMe](https://glcdn.githack.com/ckf97/my-collection-ap2019/raw/master/MiniEx05/MiniEx05_library/empty-example/index.html)

[Chosen MiniEx](https://gitlab.com/ckf97/my-collection-ap2019/tree/master/MiniEx05)

![image](MiniEx05_Flowchart.png)

I think part of the difficulty of drawing a flowchart is defining events, you have to be aware of when they happen and why they happen. Overall, I believe it's 
difficult because it asks a different kind of thinking than when just making a program. Defining events and putting a process into blocks can suddenly make it 
feel artificial, which might not have been part of one's thoughts while making the program or coming up with the idea. But the flowchart is often a help in 
organizing your thoughts on the program you're making as well as being a clear way to demonstrate a program before it has been made. 

**Group 4:**

Idea 01:

![image](Flowchart_idea01.png)

The first idea is based on what prejudices we hold. The user is essentially choosing who would be the better citizen, and will after choosing be presented 
with a picture of the person behind the characteristic. 

The techinal challenges of this program stands largely in the gathering of data. Who's pictures would we use, to use real characteristics based on real people 
or to make them up ourselves. The data needed to make this program "vibrant" could become a real challenge. 

Idea 02:

![image](Flowchart_idea02.png)

The second idea is based on the city Aarhus and creating a map that does not look like a map. The idea is to have a group of dots (or points) linked together 
across many different lines, and by clicking on a dot a picture of a specific place in Aarhus would appear. 

The technical challenges of this program lies in our coding abilities and wether we can make the program look and work the way we want to (particularly regarding clicking movable objects and making the movements controlled). 

Individual: 

In the group exercise the flowchart was about defining what the program would be. Even though we had agreed on an idea in the group, the individual might still 
have different idea, but when making the flowchart we had to agree on what the next step would be. This way we were all ablt to become more certain on the kind 
of program we wanted to make and how it would work, rather than finding that out when you're doing the actual programming. 

The use of flowcharts is done often to understand an algorithm in whatever context. The word algorithm is used quite often, even when not talking programming or code, although it's not always as easy to understand what is really meant by it. I, myself, didn't quite realize what exactly it was until recently. But I'd argue that flowcharts are almost needed for me to fully understand the meaning of it. I was under the impression that an algorithm was basically just an equation of sorts, and not, say, an entire code for a program. But flowcharts gave me the visuals to understand what was being said. In the text *The Multiple Meanings of a Flowchart*, Nathan Ensmenger compares the flowchart to boundary objects as "*an artifact that simultaneously inhabits multiple intersecting social and technical worlds.”*", which I find describes the flowchart well. The flowchart can function both as a help to the programming process as well as a help for communication and making sure everyone involved understands. There are many benefits to using a flowchart in your design process, but it is not always necessary. Which is why I believe it "failed" after it became mandatory. The same method cannot work for every process, so when it becomes mandatory to do so, it becomes a unhelpful chore rather than a useful addition to the process. 

