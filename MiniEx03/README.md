Mini Exercise 03 by Cecilie Frandsen

"Make your own throbber"

[RunMe](https://cdn.staticaly.com/gl/ckf97/my-collection-ap2019/raw/master/MiniEx03/MiniEx03_library/empty-example/index.html)

![Screenshot](MiniEx03_screenshot.JPG)

I think throbbers are mainly there for human understanding/comfort because in many cases, we don't actually need them anymore, but the fundamental understanding that something 
needs to load before we can see it, is pretty much ingrained in us. Also, if the throbber is not present when a video stops or a page is loading or anything like that, 
it would cause near panic from the user who would think something wasn't working the way it should. The throbber let's us know not that something will be happening, 
the absence of it would only make us more worried. Aside from that, it also represents how we understand the concept of time within digital culture, which is mainly that we 
don't really see it even when we know it passes. The throbber is there to show the user that time *is* passing, but not to specify *how much* time is passing, 
which I think represents pretty well how humans actually tend to spend a lot of time using digital artefacts while not noticing how much time is passing. 
Furthermore, we've learned that the throbber icon tells us to wait, and even though there's nothing on it telling us how long we have to wait, we generally know 
that we won't have to wait long and therefor we don't even worry about there being no specification. 

I've made two different throbbers with similar designs. Both resemble a panda, and the left one fades in and out, while the right moves up and down. 
I wanted to have my throbber look a little different than the usual circle of dots, which is why I went with the panda because I thought it would be cute and fun. 
My original idea for the panda throbber, was to make each part light up one by one, and have that be the throbber, but I couldn't figure out how to make that happen, 
so instead I went for a panda throbber moving up and down the screen instead of in a circle, and a panda throbber fading in and out. 
I'm quite satisfied with the results, even though I didn't make exactly what I wanted.

To make the throbbers, I used the equation from Winnie's runme, and changed some of the variables to fit with what I wanted. It did take me a while to figure out 
what parts to change because math is certainly not my strong suit, and the second half of the equation is still too complicated for me to understand how it exactly works. 
All I know is that it *does* work, which is not ideal but it is what it is. 