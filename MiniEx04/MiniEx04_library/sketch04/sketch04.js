/*sketch04, prepared by Winnie Soon
//Interacting with captured data: Mouse, Keyboard, Audio, Web Camera
check:
1. sound input via microphone: https://p5js.org/examples/sound-mic-input.html
2. dom objects like button
3. p5.sound library: https://github.com/processing/p5.js-sound/blob/master/lib/p5.sound.js
4. Face tracking library: https://github.com/auduno/clmtrackr
5. p5js + clmtracker.js: https://gist.github.com/lmccart/2273a047874939ad8ad1
*/
var button;
var ctracker;

function setup() {
  background(255);
  rectMode(CENTER);

  //web cam capture
  var capture = createCapture();
  capture.size(windowWidth,windowHeight);
  capture.position(0,0);
  capture.hide();
  //capture.hide();
  var c = createCanvas(windowWidth,windowHeight);
  c.position(0,0);

  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  button = createButton('Star Over');
  button.position(windowWidth/2-40,80);
  //mouse capture
  button.mousePressed(clearence);  //click the button to clear the screen
}

function draw() {
  fill(255);
  noStroke();
  textSize(50);
  text("you can't cover the red",50,500);
  text("you can't cover the red",50,300);
  text("you can't cover the red",50,windowHeight-30);
  text("you can't cover the red",50,50);


    noFill();
    stroke(0);
    ellipse(mouseX,mouseY,200,200);

  var positions = ctracker.getCurrentPosition();
  if (positions.length) { //check the availability of web cam tracking
    for (let i=62; i<63; i++) {  //loop through face track points (see: https://www.auduno.com/clmtrackr/docs/reference.html)
      noStroke();
      fill(map(positions[i][0], 0, width, 100, 255),0,0,10);  //color with alpha value
      //draw ellipse
      ellipse(positions[i][0], positions[i][1], 100, 100);
    }
  }
  if (mouseY > windowHeight/2) {
    stroke(0,255,0);
    noFill();
    ellipse(mouseX,mouseY,200,200);
  }
  if (mouseX > windowWidth/2 && mouseY > windowHeight/2) {
    stroke(0,0,255);
    noFill();
    ellipse(mouseX,mouseY,200,200);
  }
  if (mouseX < windowWidth/2 && mouseY < windowHeight/2) {
    stroke(200,0,150);
    noFill();
    ellipse(mouseX,mouseY,200,200);
  }
}

function clearence() {
  clear();
}
