Mini Exercise 01 by Cecilie Frandsen

On this page, music will start and the volume can be adjusted by moving the mouse up and down.

**^^^^ (actually, the sound doesn't seem to work on the staticaly link, I am not sure why.)**

When the mouse moves up and down, the background also changes color.
The rectangle on the page "bounces" off the left and right sides, and whenever it hits the right side, 
the soundrate goes up, and on the left side it goes back to normal soundrate.
If you click on the screen, it becomes more pink while clicked.

**Describing the process:**
This first coding process has mostly been about trying things out. I could say it's been about familiarzing myself with code, which of course is also true, 
but I think that will be a general thing during this entire course, so I'd rather say this week in particular has been the "trial and error" kind of week. 
I found some code in a tutorial for a bouncing circle, which I then replicated and played around with, changing parameters and adding stuff to it. But whenever I had an idea, 
I had to search for a tutorial on how to do it, which can be quite frustrating. The fact that having an idea and being able to make that idea into reality, are two very 
different realities is a hard mindset to get into. But overall, I had fun trying to mess around with it, despite the frustrations that came along with it. 

**Reading and writing:**
Like many others, I too think that coding is a language to learn. As I too have tried learning a new language that required me to learn a new alphabet, 
I find there are many similarities to learning a language and learning coding, particularly when it comes to reading and writing. 
It is technically quite easy to learn a new alphabet, because learning the alphabet is simply learning what particular sound a particular letter makes. 
Just like in coding, you learn that ellipse can make a circle, and a rect can make a square. Trouble only starts to show when you need to be able to 
read it in full and understand what it says, because knowing what one letter is by itself is very different from knowing what the word is when it is put 
together with another letter. Similarly, I can learn one syntax, maybe to draw the ellipse, and I will know what that does specifically, but I might not know what 
will happen when I put it together with another syntax because putting two together will give a different result than having one standing alone. 
So I find it similar to learning a new language, which tells me that it will take a long time and a lot of practice for me to become even close to fluent in it. 

**What coding means to me:**
Honestly? I don't know yet. It's completely new to me, and it has kind of always been this sort of faraway thing that I never really thought I would be able to touch, 
so working with it now is very odd to me. I know neither the limitations nor the full capabilities of coding well enough for me to feel like I really have a grasp on what it is.
It's a learning progress, and I hope I have a better answer to this question by the end of the semester. 

Link: https://cdn.staticaly.com/gl/ckf97/my-collection-ap2019/raw/master/MiniEx01/MiniEx1_library/empty-example/index.html

![Screenshot](MiniEx01_screenshot.JPG)
